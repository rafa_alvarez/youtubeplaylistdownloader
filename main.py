# Imports
from pytube import YouTube
from pytube import Playlist
import os
# Variables
url = str(input("Introduce el link de la playlist que quiere descargar: \n"))
playlist = Playlist(url)
# I create a dir called as the name of the playlist
# Enter your path here
path = os.path.join("", playlist.title)
os.mkdir(path)
# Playlist returns list of objects of type Youtube
for item in playlist:
    # I Download the video
    yt = YouTube(item)
    item = yt.streams.filter(only_audio=True).first()
    out_file = item.download(output_path=path)

    # I save the video
    base, ext = os.path.splitext(out_file)
    new_file = base + '.mp3'
    os.rename(out_file, new_file)

    # Print status of downloads
    print (item.title + " se ha descargado correctamente")
